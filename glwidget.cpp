#ifdef __APPLE__
#include <GLUT/glut.h>
#include <glu.h>
#else
#include <GL/glut.h>
#include <GL/glu.h>
#endif
#include "glwidget.hpp"
#include "modelerdraw.hpp"

GLWidget::GLWidget(QWidget *parent):
    QGLWidget(parent)
{
    connect(&mTimer, SIGNAL(timeout()), this, SLOT(updateGL()));
    mTimer.start(16);
}

void GLWidget::initializeGL()
{
    glClearColor(0.5, 0.5, 0.5, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glRotatef(0.5, 0, 1, 0);
    glColor3f(0.f, 1.f, 0.f);
//    glutSolidTeapot(0.6);
    glPushMatrix();
        glTranslatef(0.0, 1.0, 0.0);
        glRotatef(45, 1.0, 1.0, 1.0);
        drawRevolution(30, 1);
    glPopMatrix();

}

void GLWidget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w/h, 0.01, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);
}
