#ifndef MODELERDRAW_HPP
#define MODELERDRAW_HPP

#include <cstdio>
#include <string.h>
#include <stdlib.h>
#include <vector>

typedef struct tag_POINT_2D {
    float x;
    float y;
} Point2d;

void drawRevolution(int divisions, double scale);

#endif // MODELERDRAW_HPP
