#ifdef __APPLE__
#include <GLUT/glut.h>
#include <glu.h>
#else
#include <GL/glut.h>
#include <GL/glu.h>
#endif

#include "modelerdraw.hpp"
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "vec.hpp"

void drawRevolution(int divisions, double scale)
{
//    std::vector<Point2d> revolution_pts = *pts;
    std::vector<Point2d> revolution_pts;
    Point2d p1;
    Point2d p2;
    p1.x = 0.0; p1.y = 0.0;
    p2.x = 1.0; p2.y = 1.0;

    revolution_pts.push_back(p1);
    revolution_pts.push_back(p2);

    const float PI = 3.141592653589793238462643383279502f;

    int num_pts = revolution_pts.size();
    float tot_arc_length = 0.f;
    float min_y = 1e6;

    for (int j = 1; j < num_pts; ++j) {
        float revXn  = static_cast<float>(revolution_pts[j-1].x);
        float revX   = static_cast<float>(revolution_pts[j].x);
        float revYn  = static_cast<float>(revolution_pts[j-1].y);
        float revY   = static_cast<float>(revolution_pts[j].y);

        // find min y for moving the shape to the floor level and
        float yn = revYn * scale;
        float y = revY * scale;
        min_y = min_y < y ? (min_y < yn ? min_y : yn) : (y < yn ? y : yn);

        // calculate the total arc length for better uv mapping in v-direction
        tot_arc_length += sqrt(pow(revYn - revY, 2) +
                               pow(revXn - revX, 2));
    }

    // get the offset from the floor level to move the object
    float floor_y = -2;
    float offset = floor_y - min_y;

    // declaration of vectors to hold data for glDrawElements
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texture_uv;
    std::vector<GLuint> indices;

    float cur_arc_length = 0.f;
    for (int j = 0; j < num_pts; ++j) {
        float revX   = static_cast<float>(revolution_pts[j].x);
        float revY   = static_cast<float>(revolution_pts[j].y);
        float x = revX * scale;
        float y = revY * scale + offset;

        // get xp, yp for Tangent1 calculations, revert if at the end
        float xp = 0.f;
        float yp = 0.f;
        if (j+1 != num_pts) {
            xp = static_cast<float>(revolution_pts[j+1].x * scale);
            yp = static_cast<float>(revolution_pts[j+1].y * scale + offset);
        }
        else {
            xp = static_cast<float>(revolution_pts[j-1].x * scale);
            yp = static_cast<float>(revolution_pts[j-1].y * scale + offset);
        }

        // calculate the current arc length for better uv mapping in v-dir
        if (j != 0) {
            cur_arc_length += sqrt(pow(revY - revolution_pts[j-1].y, 2) +
                                   pow(revX - revolution_pts[j-1].x, 2));
        }

        float v = 0.f;
        v = cur_arc_length/tot_arc_length;

        // declaration of tangents and normal vec for per-vertex normals
        // in addition to current and next vertex vectors
        auto tangent1  = Vec3f(0.f, 0.f, 0.f);
        auto tangent2  = Vec3f(0.f, 0.f, 0.f);
        auto normalVec = Vec3f(0.f, 0.f, 0.f);
        auto vertVec   = Vec3f(0.f, 0.f, 0.f);
        auto vertVecP  = Vec3f(0.f, 0.f, 0.f);

        for (int k = 0; k <= divisions; ++k) {
            float cosK = std::cos(2*PI*static_cast<float>(k)/divisions);
            float sinK = std::sin(2*PI*static_cast<float>(k)/divisions);
            vertVec    = Vec3f(cosK*x, y, -sinK*x);
            vertVecP   = Vec3f(cosK * xp, yp, -sinK*xp);
            float u    = static_cast<float>(k)/divisions;

            tangent1 = vertVecP - vertVec;
            tangent2 = Vec3f(sinK, 0.f, cosK);

            // The case when Tangent1 is reverted, Tangent2 reverted here
            if (j+1 == num_pts) {
                tangent2 = -tangent2;
            }

            normalVec = tangent2 ^ tangent1;
            normalVec.normalize();

            // If curve control points are on -x, flip the normal vector.
            // If px and pz were to be reverted, textures would rotate too,
            // which is not what we want according to the example solution.
            if (x < 0) {
                normalVec = -normalVec;
            }

            for (size_t i = 0; i < 3; ++i) {
                normals.push_back(normalVec[i]);
                vertices.push_back(vertVec[i]);
            }

            texture_uv.push_back(u);
            texture_uv.push_back(v);

            if (j < num_pts-1) {
                if (k < divisions) {
                    indices.push_back(divisions*j+k);
                    indices.push_back(divisions*j+k+1);
                    indices.push_back(divisions*(j+1)+k+1);
                    indices.push_back(divisions*j+k+1);
                    indices.push_back(divisions*(j+1)+k+2);
                    indices.push_back(divisions*(j+1)+k+1);
                }
            }
        }
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
    glNormalPointer(GL_FLOAT, 0, &normals[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &texture_uv[0]);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT,
                   &indices[0]);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}
