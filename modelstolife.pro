#-------------------------------------------------
#
# Project created by QtCreator 2014-11-24T16:24:42
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = modelstolife
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp \
    modelerdraw.cpp

HEADERS  += \
    mainwindow.hpp \
    glwidget.hpp \
    vec.hpp \
    modelerdraw.hpp

FORMS    += mainwindow.ui

CONFIG += c++11

macx {
    LIBS += -framework GLUT
}

unix:!macx {
    LIBS += -lglut -lGL -lGLU
}

win32 {
    LIBS += -lglut -lGL -lGLU
}

OTHER_FILES += \
    .gitignore
